# sis8300drv-dkms

CentOS RPM for [sis8300drv](https://bitbucket.org/europeanspallationsource/m-kmod-sis8300) kernel module: driver for Struck sis8300 DAQ card 

This RPM includes:

- the source for the kernel module managed by dkms (sis8300drv)
- udev rule to give read/write access to all users

To build a new version of the RPM:

- update the version in the spec file
- push your changes to check that the RPM can be built by gitlab-ci
- tag the repository and push the tag for the RPM to be uploaded to artifactory rpm-ics repo

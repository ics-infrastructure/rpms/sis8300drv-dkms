%define module_name sis8300drv
%define version 4.10.2
%define src_dir sis8300drv-%{version}

Name:           %{module_name}-dkms
Version:        %{version}
Release:        0
Summary:        DKMS-ready kernel-source for the sis8300 driver
License:        GPL
URL:            https://gitlab.esss.lu.se/epics-modules/rf/sis8300drv
Source:         https://gitlab.esss.lu.se/epics-modules/rf/sis8300drv/-/archive/%{version}/sis8300drv-%{version}.tar.gz
BuildRequires:  autoconf libtool gcc gcc-c++
Requires(pre):  dkms
Requires(post): dkms
Buildroot:      %{_tmppath}/%{name}-%{version}-root

%description
Kernel module driver for Struck sis8300 DAQ card

%prep
%setup -qn %{src_dir}

%build

%clean
rm -fr $RPM_BUILD_ROOT

%install
# Copy all sources required for dkms to build the module
mkdir -p %{buildroot}/usr/src/%{module_name}-%{version}-%{release}
cp -a src/main/c/driver/* src/main/c/include/*.h %{buildroot}/usr/src/%{module_name}-%{version}-%{release}/

# Create udev rule to give normal users read/write access
mkdir -p ${RPM_BUILD_ROOT}/etc/udev/rules.d
echo 'KERNEL=="sis8300-[0-9]*", MODE="0666"' > ${RPM_BUILD_ROOT}/etc/udev/rules.d/99-sis8300drv.rules

# Create dkms.conf file
cat > %{buildroot}/usr/src/%{module_name}-%{version}-%{release}/dkms.conf <<EOF

PACKAGE_NAME="%{module_name}"
PACKAGE_VERSION="%{version}-%{release}"

MAKE[0]="make -C \${kernel_source_dir} SUBDIRS=\${dkms_tree}/\${PACKAGE_NAME}/\${PACKAGE_VERSION}/build modules"
CLEAN="make -C \${kernel_source_dir} SUBDIRS=\${dkms_tree}/\${PACKAGE_NAME}/\${PACKAGE_VERSION}/build clean"

BUILT_MODULE_NAME[0]="%{module_name}"
DEST_MODULE_LOCATION[0]="/kernel/drivers/%{module_name}"

AUTOINSTALL="yes"
REMAKE_INITRD="no"
EOF

%post
%udev_rules_update
dkms add -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade
dkms build -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade
dkms install -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade

%preun
dkms remove -m %{module_name} -v %{version}-%{release} --rpm_safe_upgrade --all ||:

%postun
%udev_rules_update

%files
%defattr(-,root,root)
/usr/src/%{module_name}-%{version}-%{release}
/etc/udev/rules.d/99-sis8300drv.rules

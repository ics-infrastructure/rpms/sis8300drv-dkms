#!/bin/bash
set -e

# Download sources
mkdir -p SOURCES
spectool -g -C ./SOURCES SPECS/sis8300drv-dkms.spec
# Build RPM
rpmbuild -v --define "%_topdir `pwd`" -bb SPECS/sis8300drv-dkms.spec

tree RPMS

# Remove unwanted RPM
rm -f RPMS/x86_64/sis8300drv-dkms-debuginfo-*.rpm
